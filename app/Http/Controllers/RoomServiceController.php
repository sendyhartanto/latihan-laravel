<?php

namespace App\Http\Controllers;
use Illuminate\support\Facades\Input;
use Illuminate\Http\Request;
use App\room_types;
use App\room_services;
use App\room;
use App\Http\Controllers\Controller;


class RoomServiceController extends Controller
{
    public function Laporan(Request $request){
    	$type=room_types::where('active','1')->get();
    	$start=Input::get('start');
    	$end=Input::get('end');
    	$types=Input::get('room_type');

    	if ($start!="" && $end=="" && $types=="" ) {
    		$type=room_types::where('active','1')->get();

	    	$room=room::where('created_at','LIKE','%'.$start.'%')->get();
	    	$room_type=room_types::all();
	    	return view('laporan',compact('type','room','room_type'));
    	}
    	elseif ($start=="" && $end!="" && $types=="" ) {
    		$type=room_types::where('active','1')->get();

	    	$room=room::where('created_at','LIKE','%'.$extends.'%')->get();
	    	$room_type=room_types::all();
	    	return view('laporan',compact('type','room','room_type'));
    	}
    	elseif ($start=="" && $end=="" && $types!="" ) {
    		$type=room_types::where('active','1')->get();
	    	$room=room::where('room_type_id',$types)->get();

	    	$room_type=room_types::all();
	    	return view('laporan',compact('type','room','room_type'));
    	}
    	elseif ($start!="" && $end!="" && $types=="" ) {
    		$type=room_types::where('active','1')->get();
    		$room=room::whereBetween('created_at',[$start,$end])->get();

    		$room_type=room_types::all();
	    	return view('laporan',compact('type','room','room_type'));
    	}
    	elseif ($start!="" && $end=="" && $types!="" ) {
    		$type=room_types::where('active','1')->get();
	    	$room=room::where('room_type_id',$types)->where('created_at','LIKE','%'.$start.'%')->get();

	    	$room_type=room_types::all();
	    	return view('laporan',compact('type','room','room_type'));
    	}
    	elseif ($start=="" && $end!="" && $types!="" ) {
    		$type=room_types::where('active','1')->get();
	    	$room=room::where('room_type_id',$types)->where('created_at','LIKE','%'.$end.'%')->get();

	    	$room_type=room_types::all();
	    	return view('laporan',compact('type','room','room_type'));
    	}elseif($start!="" && $end!="" && $types!=""){
    		$type=room_types::where('active','1')->get();
    		$room=room::whereBetween('created_at',[$start,$end])->where('room_type_id',$types)->get();
    		$room_type=room_types::all();
	    	return view('laporan',compact('type','room','room_type'));
    	}
    	else{
			$type=room_types::where('active','1')->get();
	    	$room=room::all();

	    	$room_type=room_types::all();
	    	return view('laporan',compact('type','room','room_type'));
    	}
    }

    public function reset(){
    	return redirect('laporan');
    }

}