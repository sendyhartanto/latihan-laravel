<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\facilities;
use App\room_types;
use App\room;
use App\room_services;
use Illuminate\support\Facades\Input;

class RoomController extends Controller
{
    public function index(){
    	$room=room::all();

    	return view('room.index',compact('room'));
    }

    public function create(){
    	$room=room_types::where('active','1')->get();

    	$facilities=facilities::where('active','1')->get();
    	
    	return view('room.create',compact('room','facilities'));
    }

    public function store(Request $request){
    	$this->validate($request,[
    		'name'=>'required',
    		'type'=>'required',
    	]);

    	$check=room::where('name',$request->name)->exists();
    	if ($check) {
    		return back()->with('warning','Maaf data sudah ada !');
    	}else{

    		if (!$request['active']) {
    			$request['active']="0";
	    	}

    		room::create([
    			'room_type_id'=>$request->type,
    			'name'=>$request->name,
    			'active'=>$request['active'],
    			'description'=>$request->description,
    		]);

    		$jumlah=count($request->checkbox);

    		if ($jumlah<=0) {
    			room_services::create([
	    			'room_id'=>$request->type,
	    			'facility_id'=>"0",
    			]);	
    		}else{
	    		for($x=0 ;  $x<$jumlah ; $x++) {
	    			room_services::create([
		    			'room_id'=>$request->type,
		    			'facility_id'=>$request->checkbox[$x],
	    			]);	
	    		}
    		}    		

    		return redirect('/room')->with('pesan','Data berhasil ditambahkan');
    	}
    }

    public function Edit($id){
    	$room=room_types::where('active','1')->get();

    	$facilities=facilities::where('active','1')->get();

    	$data=room::where('room_type_id',$id)->first();

    	$service=room_services::where('room_id',$id)->get();
    	$limit=room_services::where('room_id',$id)->count();
    	
    	return view('room.edit',compact('room','facilities','data','service','limit'));
    }

    public function update(Request $request,$id){
    	$this->validate($request,[
    		'name'=>'required',
    		'type'=>'required',
    	]);  	

			if (!$request['active']) {
	    		$request['active'] = 0;
	    	}

    		room::where('room_type_id',$id)->update([
    			'room_type_id'=>$request->type,
    			'name'=>$request->name,
    			'active'=>$request['active'],
    			'description'=>$request->description,
    		]);

    		$jumlah=count($request->checkbox);

    		if ($jumlah<=0) {
    			room_services::where('room_id',$id)->update([
		    			'room_id'=>"0",
		    			'facility_id'=>"0",
	    			]);	
    		}else{
	    		for($x=0 ;  $x<$jumlah ; $x++) {
	    			room_services::where('room_id',$id)->update([
		    			'room_id'=>$request->type,
		    			'facility_id'=>$request->checkbox[$x],
	    			]);	
	    		}  
    		}
    		return redirect('room')->with('pesan','Data berhasil diupdate');
    }

    public function delete($id){
    	room::where('room_type_id',$id)->delete();
    	room_services::where('room_id',$id)->delete();

    	return redirect('room')->with('pesan','Data berhasil dihapus');
    }

}