<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\room_types;
use App\facilities;
class RoomTypeController extends Controller
{
	public function index(){
		$room=room_types::all();

		return view('room_types.index',compact('room'));
	}

    public function create(){
    	return view('room_types.create');
    }

    public function Edit($id){
    	$data=room_types::where('id',$id)->first();
    	return view('room_types.edit',compact('data'));
    }

    public function Update(Request $request,$id){
    	$this->validate($request,[
    		'name'=>'required',
    	]);  	

			if (!$request['active']) {
	    		$request['active'] = 0;
	    	}

    		$data=room_types::where('id',$id)->update([
    			'name'=>$request->name,
    			'active'=>$request['active'],
    		]);
    		return redirect('room_types')->with('pesan','Data berhasil diupdate');
    }

    public function Delete($id){
    	room_types::where('id',$id)->delete();
    	return redirect('room_types')->with('pesan','Data berhasil dihapus');
    }

    public function store(Request $request){
    	$this->validate($request,[
    		'name'=>'required',
    	]);

    	$check=room_types::where('name',$request->name)->exists();
    	if ($check) {
    		return back()->with('warning','Maaf data sudah ada !');
    	}else{
    		if (!$request['active']) {
    			$request['active']="0";
	    	}

    		room_types::create([
    			'name'=>$request->name,
    			'active'=>$request['active'],
    		]);

    		return redirect('room_types')->with('pesan','Data berhasil ditambahkan');
    	}
    }
}


