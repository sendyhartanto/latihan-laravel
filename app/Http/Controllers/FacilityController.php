<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\facilities;

class FacilityController extends Controller
{
	public function index(){
    	$facilities=facilities::all();

    	return view('facilities.index',compact('facilities'));
    }

    public function create(){
    	return view('facilities.create');
    }

    public function Edit($id){
    	$data=facilities::where('id',$id)->first();
    	return view('facilities.edit',compact('data'));
    }

    public function Update(Request $request,$id){
    	$this->validate($request,[
    		'name'=>'required',
    	]);  	

			if (!$request['active']) {
    			$request['active']="0";
	    	}

    		$data=facilities::where('id',$id)->update([
    			'name'=>$request->name,
    			'active'=>$request['active'],
    		]);
    		return redirect('facilities')->with('pesan','Data berhasil diupdate');
    }

    public function Delete($id){
    	facilities::where('id',$id)->delete();
    	return redirect('facilities')->with('pesan','Data berhasil dihapus');
    }

    public function store(Request $request){
    	$this->validate($request,[
    		'name'=>'required',
    	]);

    	$check=facilities::where('name',$request->name)->exists();
    	if ($check) {
    		return back()->with('warning','Maaf data sudah ada !');
    	}else{
    		if (!$request['active']) {
    			$request['active']="0";
	    	}

    		facilities::create([
    			'name'=>$request->name,
    			'active'=>$request['active'],
    		]);

    		return redirect('facilities')->with('pesan','Data berhasil ditambahkan');
    	}
    }
}
