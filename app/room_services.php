<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class room_services extends Model
{
    protected $table="room_services";
    protected $fillable=['room_id','facility_id'];
}
