<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class room extends Model
{
	protected $table="rooms";
    protected $fillable=['room_type_id','name','type','description','active'];
}
