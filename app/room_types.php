<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room_types extends Model
{
	protected $table='room_types';

    protected $fillable=[
    	'name','active',
    ];
}
