<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

//facilities
Route::get('/facilities','FacilityController@index');
Route::get('/facilities/create','FacilityController@create');
Route::post('/facilities/store','FacilityController@store');
Route::get('/facilities/edit/{id}','FacilityController@Edit');
Route::get('/facilities/delete/{id}','FacilityController@Delete');
Route::patch('/facilities/update/{id}','FacilityController@Update');

//room_types
Route::get('/room_types','RoomTypeController@index');
Route::get('/room_types/create','RoomTypeController@create');
Route::post('/room_types/store','RoomTypeController@store');
Route::get('/room_types/edit/{id}','RoomTypeController@Edit');
Route::get('/room_types/delete/{id}','RoomTypeController@Delete');
Route::patch('/room_types/update/{id}','RoomTypeController@Update');

//room
Route::get('/room','RoomController@index');
Route::get('/room/create','RoomController@create');
Route::post('/room/store','RoomController@store');
Route::get('/room/edit/{id}','RoomController@edit');
Route::get('/room/delete/{id}','RoomController@delete');
Route::patch('/room/update/{id}','RoomController@update');

//laporan
Route::get('/laporan','RoomServiceController@Laporan');
Route::post('/laporan','RoomServiceController@Laporan');
Route::get('/reset','RoomServiceController@reset');

//home
Route::get('/home','HomeController@index')->name('home');