@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        	@if(session('pesan'))
            	<div class="alert alert-success alert-dismissible">
            	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> 
            		{{ session('pesan') }}
            	</div>

            @elseif(session('warning'))
            	<div class="alert alert-warning alert-dismissible"> 
            		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            		{{ session('warning') }}
            	</div>

            @endif
            <br>
            <div class="card">
                <div class="card-header" style="background-color:#2C3E50; color: white; ">{{ __('Form input fasilitas kamar') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ URL::to('/facilities/store') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-sm-2 col-form-label text-md-right">{{ __('Name ') }}</label>

                            <div class="col-md-8">
                                <input id="name" type="text" name="name" class=" form-control col-md-12" required autofocus>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-2">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="1" checked="" name="active"> {{ __('Active') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-8">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Save') }}
                                </button>

                                <a href="{{URL::to('/facilities')}}" class="btn btn-warning">Back</a>
                                	
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
