@extends('layouts.sidebar')

@section('content')


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-9 offset-md-2">
            <div class="card">
                <div class="card-header" style="background-color:#2C3E50; color: white; ">{{ __('Form Laporan') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ URL::to('/laporan') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="sdate" class="col-sm-2 col-form-label text-md-right">{{ __('Start Date ') }}</label>

                            <div class="col-md-8">
                                <input id="sdate" type="date" name="start" value="{{@$_POST['start']}}" class="form-control col-md-12" autofocus>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="edate" class="col-sm-2 col-form-label text-md-right">{{ __('End Date ') }}</label>

                            <div class="col-md-8">
                                <input type="date" id="edate" name="end" value="{{@$_POST['end']}}" class="form-control col-md-12">
                            </div>
                        </div>
						<div class="form-group row">
                            <label for="rtype" class="col-sm-2 col-form-label text-md-right">{{ __('Room Type ') }}</label>

                            <div class="col-md-8">
                                <select id="rtype" name="room_type" class="form-control">
                                	<option></option>>
                                	@foreach($type as $types)
                                		<option value="{{$types->id}}">{{$types->name}}</option>	
                                	@endforeach
                                	
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-8 offset-md-8">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Submit') }}
                                </button>        

                                <a href="{{url('reset')}}" class="btn btn-warning">Reset</a>                  	
                            </div>
                        </div>

                        <div class="form-group row">
                        	<table class="table table-stripped table-hover table-bordered"> 
                        		<thead> 
                        			<td>Name</td>
                        			<td>Type</td>
                        			<td>Created At</td>
                        		</thead>

                        		<tbody>      
                        		@foreach($room as $rooms)                  			
                        			<tr> 
	                        			<td>{{$rooms->name}}</td>
	                        			<td>	
	                        				@foreach($room_type as $room_types)
		                        				@if($rooms->room_type_id==$room_types->id)
			                        				{{$room_types->name}}
			                        			@endif
		                        			@endforeach	
	                        			</td>
	                        			<td>{{$rooms->created_at}}</td>
                        			</tr>
                        		@endforeach
                        		</tbody>
                        	</table>
						</div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
