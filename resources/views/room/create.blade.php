@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-9">

        	@if(session('pesan'))
            	<div class="alert alert-success alert-dismissible"> 
            		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            		{{ session('pesan') }}
            	</div>
            	<br>
            @elseif(session('warning'))
            	<div class="alert alert-warning alert-dismissible">
            	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> 
            		{{ session('warning') }}
            	</div>
            	<br>
            @endif
            <div class="card">
                <div class="card-header" style="background-color:#2C3E50; color: white; ">{{ __('Form input ruangan') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ URL::to('/room/store') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-sm-2 col-form-label text-md-right">{{ __('Name ') }}</label>

                            <div class="col-md-8">
                                <input id="name" type="text" name="name" class=" form-control col-md-12" required autofocus>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label text-md-right">{{ __('Room Type ') }}</label>

                            <div class="col-md-8">
                                <select name="type" class="form-control">
                                	@foreach($room as $room1)
                                	
                                		<option value="{{ $room1->id }}">{{ $room1->name }}</option>
                                	
                                	@endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-sm-2 col-form-label text-md-right">Description</label>
                            
                            <div class="col-md-8">
                                <textarea name="description" class="form-control"></textarea>	
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-2">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="1" checked="" name="active"> Active
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-8 offset-md-2">
                                    <label>
                                        Facilities
                                    </label>
                                <br>
                                <table class="table table-hover table-bordered"> 
                                	<thead> 
                                		<td>Facilities</td>
                                		<td>Choice</td>
                                	</thead>
                                	<tbody> 
                                		@foreach($facilities as $facilities1)
	                                		<tr> 
	                                			<td>{{ $facilities1->name }}</td>
	                                			<td>
	                                				<input type="checkbox" name="checkbox[]" value="{{ $facilities1->id }}">  
	                                			</td>
	                                		</tr>
                                		@endforeach
                                	</tbody>
                                </table>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-8">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Save') }}
                                </button>

                                <a href="{{URL::to('/room')}}" class="btn btn-warning">Back</a>
                                	
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
