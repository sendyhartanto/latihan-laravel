@extends('layouts.sidebar')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-9 offset-md-2">
        	@if (session('status'))
                <div class="alert alert-success alert-dismissible">
                	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            	    {{ session('status') }} , You are logged in!
                </div>
            @endif

            @if(session('pesan'))
            	<div class="alert alert-success alert-dismissible">
            		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            	    {{ session('pesan') }}
                </div>
            @endif
            <br>
            <div class="card">
                <div class="card-header">
                	<div class="row">
                		<div class="col-md-9">
                			<label>Data tipe ruangan</label>
                		</div>
                		<div class="col-md-1">
                			<a href="{{ url('room/create') }}" class="btn btn-primary">Tambahkan Data</a>
                		</div>
                	</div>
            	</div>

                <div class="card-body">
                    <table class="table table-hover table-bordered"> 
                    	<thead> 
                    		<td>No</td>
                    		<td>Nama jenis</td>
                    		<td>Status</td>
                    		<td>Description</td>
                    		<td>Action</td>
                    	</thead>
                    	<tbody>    
                    	@foreach($room as $room1)               	
                    		<tr> 
                    			
                    			<td>{{ $loop->index+1 }}</td>
                    			<td>{{ $room1->name }}</td>
                    			<td> 
                    				{{ status($room1->active) }}
                    			</td>
                    			<td>{{ $room1->description }}</td>
                    			<td> 
                    				<a href="{{ url('room/edit/'.$room1->room_type_id) }}" class="btn btn-warning">Edit</a>
                    				<a href="{{ url('room/delete/'.$room1->room_type_id )}}" onclick="return confirm('Yakin ingin dihapus?')" class="btn btn-danger">Hapus</a>
                    			</td>
                    			
                    		</tr>  
                    	@endforeach                  	
                    	</tbody>
                    </table>              
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
