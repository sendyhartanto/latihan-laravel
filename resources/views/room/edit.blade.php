@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-9">

        	@if(session('pesan'))
            	<div class="alert alert-success alert-dismissible"> 
            		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            		{{ session('pesan') }}
            	</div>
            	<br>
            @elseif(session('warning'))
            	<div class="alert alert-warning alert-dismissible">
            	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> 
            		{{ session('warning') }}
            	</div>
            	<br>
            @endif
            <div class="card">
                <div class="card-header" style="background-color:#2C3E50; color: white; ">{{ __('Form edit ruangan') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ URL::to('/room/update/'.$data['id']) }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-sm-2 col-form-label text-md-right">{{ __('Name ') }}</label>

                            <div class="col-md-8">
                                <input id="name" value="{{ $data['name'] }}" type="text" name="name" class=" form-control col-md-12" required autofocus>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label text-md-right">{{ __('Room Type ') }}</label>

                            <div class="col-md-8">             
                                <select name="type" class="form-control">
                                	@foreach($room as $room1)
                                		@if($room1->id==$data['room_type_id'])                 	
                             				<option value="{{ $room1->id }}" selected="">
                                				{{ $room1->name }}
                                			</option>
                                		@else
                                			<option value="{{ $room1->id }}">
                                				{{ $room1->name }}
                                			</option>
                                		@endif
                                	@endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-sm-2 col-form-label text-md-right">Description</label>
                            
                            <div class="col-md-8">
                                <textarea name="description" class="form-control">{{ $data['description'] }}</textarea>	
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-2">
                                <div class="checkbox">
                                    <label>
                                        @if($data['active']=="1")
                                    		<input type="checkbox" checked="" value="1" name="active"> {{ __('Active') }}
                                    	@else
                                    		<input type="checkbox" value="1" name="active"> {{ __('Active') }}
                                    	@endif
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-8 offset-md-2">
                                    <label>
                                        Facilities
                                    </label>
                                <br>
                                <table class="table table-hover table-bordered"> 
                                	<thead> 
                                		<td>Facilities</td>
                                		<td>Choice</td>
                                	</thead>
                                	<tbody>
                                	@foreach($facilities as $facilities1) 
                                		
	                                		<tr> 
	                                			<td>{{ $facilities1['name'] }}  </td>
	                                			<td>
	                                				@foreach($service as $services)
		                                				@for($no=1;$no<$limit;$no++)
		                                					@if($services['facility_id']==$facilities1['id'])
		                                					<input type="checkbox" name="checkbox[]" value="{{ $services['facility_id'] }}" checked="">
				                                			@else
				                                				<input type="checkbox" name="checkbox[]" value="{{ $services['facility_id'] }}">
				                                			@endif
		                                				@endfor 
	                                				@endforeach

	                                				@if($services['facility_id']==$facilities1['id'])
		                                				<input type="checkbox" name="checkbox[]" value="{{ $services['facility_id'] }}" checked="">
				                                	@else
				                                		<input type="checkbox" name="checkbox[]" value="{{ $services['facility_id'] }}">
				                                	@endif
	                                			</td>
	                                		</tr>
                                		
                                	@endforeach
                                	</tbody>
                                </table>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-8">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Update') }}
                                </button>

                                <a href="{{URL::to('/room')}}" class="btn btn-warning">Back</a>
                                	
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
